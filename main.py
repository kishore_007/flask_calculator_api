from flask import Flask, render_template, request, url_for,redirect
from flask_pymongo import PyMongo


Flask_App = Flask(__name__) # Creating our Flask Instance

#connecting to a db named calculation_db
mongodb_client = PyMongo(Flask_App, uri="mongodb://localhost:27017/calculation_db")
db = mongodb_client.db


@Flask_App.route('/', methods=['GET','POST'])
def index():
    """ Displays the index page accessible at '/' """
    return render_template('index.html')

@Flask_App.route('/calculation/', methods=['POST'])
def calculation():
    """ Where we send calculator form input """
    first_input = request.form['Num1']
    second_input = request.form['Num2']
    operation = request.form['operation']

    try:
        input1 = float(first_input)
        input2 = float(second_input)

        if operation == '+':
            result = input1 + input2
            method_name = 'add'

        elif operation == '/':
            result = input1 / input2
            method_name = operation = 'div'

        else :
            result = input1 * input2
            method_name = 'mul'

        calculation_done = {'input1': input1, 'input2': input2, 'operation': operation, 'result': result}
        db.calculations.insert_one(calculation_done) #inserting document into the collection named calculations

        return redirect(url_for(method_name, number1=input1, number2=input2,
                                result=result, operation=operation))

    except ValueError:
        return render_template( 'index.html',
                                    input1=first_input,
                                    input2=second_input,
                                    operation=operation,
                                    result="Bad Input",
                                    calculation_success=False,
                                    error="Cannot perform numeric operations with provided input"
                                )
    except ZeroDivisionError:
        return render_template( 'index.html',
                                    input1=first_input,
                                    input2=second_input,
                                    operation=operation,
                                    result="Bad Input",
                                    calculation_success=False,
                                    error="Cannot perform division with zero as Denominator"
                                )


@Flask_App.route('/add/<number1>/<number2>/<result>/<operation>', methods=['GET','POST'])
def add(number1,number2,result,operation):
    """ Where we send calculator form input """
    return render_template('index.html',
                                    input1=number1,
                                    input2=number2,
                                    operation=operation,
                                    result=result,
                                    calculation_success=True)

@Flask_App.route('/div/<number1>/<number2>/<result>/<operation>', methods=['GET','POST'])
def div(number1,number2,result,operation):
    """ Where we send calculator form input """
    return render_template('index.html',
                                    input1=number1,
                                    input2=number2,
                                    operation=operation,
                                    result=result,
                                    calculation_success=True)

@Flask_App.route('/mul/<number1>/<number2>/<result>/<operation>', methods=['GET','POST'])
def mul(number1,number2,result,operation):
    """ Where we send calculator form input """
    return render_template('index.html',
                                    input1=number1,
                                    input2=number2,
                                    operation=operation,
                                    result=result,
                                    calculation_success=True)

if __name__ == '__main__':
    Flask_App.run(debug=True)

