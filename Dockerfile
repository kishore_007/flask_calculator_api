FROM python:3.9.6

RUN pip install virtualenv

ENV VIRTUAL_ENV = /calc_env

RUN virtualenv calc_env -p python

ENV PATH="VIRTUAL_ENV/bin:$PATH"

WORKDIR /f_calc

ADD . /f_calc

RUN pip install -r requirements.txt

COPY . /f_cal

CMD ["python","main.py"]
